import React from 'react'

export default function Skills() {
  return (
    <div className='skill mt-5'>
      <section id="skills" class="skills" >
				<div class="skill-content">
					<div class="section-heading text-center">
						<h4  style={{textTransform:'uppercase', fontWeight:'bold'}}>skills</h4>
					</div>
          <br/>
          <hr/>
					<div class="container">
						<div class="row mt-5">
							<div class="col-md-6">
								<div class="single-skill-content">
									<div class="barWrapper">
										<span class="progressText">adobe photoshop</span>
										<div class="single-progress-txt mt-3 d-flex">
											<div class="progress " style={{height:8, width:'80%'}}>
												<div class="progress-bar"
                         role="progressbar"
                          aria-valuenow="90"
                           aria-valuemin="10" 
                           aria-valuemax="100" 
                           style={{width:'90%',height:8,  background: '#b636ff'}}>
													  
												</div>
											</div>
											<h3>90%</h3>	
										</div>
									</div>

									<div class="barWrapper">
										<span class="progressText">adobe illustrator</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8 , width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="10" aria-valuemax="100" style={{width:'85%',height:8,  background: '#b636ff'}}>
												    
											   </div>
											</div>
											<h3>85%</h3>	
										</div>
									</div>
									<div class="barWrapper">
										<span class="progressText">adobe after effects</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8 , width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="97" aria-valuemin="10" aria-valuemax="100" style={{width:'97%',height:8,  background: '#b636ff'}}>
												   
											   </div>
											</div>
											<h3>97%</h3>	
										</div>
									</div>
									<div class="barWrapper">
										<span class="progressText">sketch</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8, width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="10" aria-valuemax="80" style={{width:'90%',height:8,  background: '#b636ff'}}>
												    
											   </div>
											</div>
											<h3>90%</h3>	
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="single-skill-content">
									<div class="barWrapper">
										<span class="progressText">html 5</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8 , width:'80%'}}>
												<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="10" aria-valuemax="100" style={{width:'90%',height:8,  background: '#b636ff'}}>
													
												</div>
											</div>
											<h3>90%</h3>	
										</div>
									</div>
									<div class="barWrapper">
										<span class="progressText">css 3 animation</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8 , width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="8" aria-valuemax="100" style={{width:'85%',height:8,  background: '#b636ff'}}>
												    
											   </div>
											</div>
											<h3>85%</h3>	
										</div>
									</div>
									<div class="barWrapper">
										<span class="progressText">communication</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress " style={{height:8, width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="97" aria-valuemin="10" aria-valuemax="100" style={{width:'97%',height:8,  background: '#b636ff'}}>
												   
											   </div>
											</div>
											<h3>97%</h3>	
										</div>
									</div>
									<div class="barWrapper">
										<span class="progressText"> creativity</span>
										<div class="single-progress-txt  mt-3 d-flex">
											<div class="progress "  style={{height:8 , width:'80%'}}>
											   <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="10" aria-valuemax="100" style={{width:'90%',height:8,  background: '#b636ff'}}>
												    
											   </div>
											</div>
											<h3>90%</h3>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>		
				</div>

		</section>
    </div>
  )
}
