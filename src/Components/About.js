import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FaFacebookF} from 'react-icons/fa'
import {FaInstagramSquare} from 'react-icons/fa'
import {BsTwitter} from 'react-icons/bs'
import {TbWorld} from 'react-icons/tb'


export default function About() {
  return (
    <div>
        
      <h3 style={{textTransform:'uppercase', color:'#6f6f6f'}}>  A Propos de Moi</h3>  
      <br/>
     
      <hr/>

      <div className="container mt-5">
				<div className="about-content">
					<div className="row mt-5">
						<div className="col-sm-6 mt-4">
							<div className="about mt-4">
								<h3 >
									I am a Professional UI/UX Designer and Web developer.
                                     Consectetur an adipisi elita, sed do eiusmod tempor incididunt ut labore 
                                     et dolore magna aliqua. Ut enim ad minim veniam quis nostrud.
								</h3>
								<p>
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                                     eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
                                     sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut 
                                     perspi unde omnis iste natus error sit voluptatem accusantium doloremque
                                      lauda ntium, totam rem aperiam, eaque ipsa quae ab illo inventore
                                       veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                                        Nemo enim ipsam vo luptatem quia voluptas sit aspernatur aut odit aut
                                         fugit,
								</p>
								<div className="row">
									<div className="col-sm-4">
										<div className="about-info">
											<h3>phone</h3>
											<p classNameName='para'>987-123-6547</p>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="about-info">
											<h3 style={{textAlign:'start'}}>email</h3>
											<p  classNameName='para'>browny@info.com</p>
										</div>
									</div>
									<div className="col-sm-4">
										<div className="about-info">
											<h3>website</h3>
											<p  classNameName='para'>www.brownsine.com</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="col-sm-offset-1 col-sm-6  justify-content-center">
							<div className="single-about-img ">
								<img src="https://technext.github.io/browny/assets/images/about/profile_image.jpg" alt="profile_image" style={{marginRight:'auto', marginLeft:'auto'}}/>
								<div className="about-list-icon">
									<ul>
										<li>
											<a href="#">
											<FaFacebookF/>
											</a>
										</li>
										<li>
											<a href="#">
                                            <TbWorld/>
											</a>
											
										</li>
										<li>
											<a href="#">
												<BsTwitter/>
											</a>
											
										</li>
										<li>
											<a href="#">
												<BsLinkedin/>
											</a>
										</li>
										<li>
											<a href="#">
												<FaInstagramSquare/>
											</a>
										</li>
										
										
									</ul>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
    </div>
  )
}
