import React from 'react'

export default function Education() {
  return (
    <div className='etudes mt-5'>
       <section id="education" class="education" style={{textTransform:'uppercase'}}>
			<div class="section-heading text-center">
				<h4  style={{textTransform:'uppercase', fontWeight:'bold'}}>education</h4>
			</div>
			<hr/>
			<div class="container mt-5">
				<div class="education-horizontal-timeline">
					<div class="row justify-content-start">
						<div class="col-sm-4">
							<div class="single-horizontal-timeline ">
								<div class="experience-time">
									<h2 style={{  textAlign: 'start', fontSize:20}}>2008 - 2010</h2>
									<h3>master <span>of </span> computer science</h3>
								</div>
								<div class="timeline-horizontal-border">
									<i class="fa fa-circle" aria-hidden="true"></i>
									<span class="single-timeline-horizontal"></span>
								</div>
								<div class="timeline">
									<div class="timeline-content">
										<h4 class="title"  style={{  textAlign: 'start', fontSize:20}}>
											university of north carolina
										</h4>
										<p style={{  textAlign: 'start', fontSize:14}}>north carolina, USA</p>
										<p class="description">
											Duis aute irure dolor in reprehenderit in vol patate velit esse cillum dolore eu fugiat nulla pari. Excepteur sint occana inna tecat cupidatat non proident. 
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-horizontal-timeline">
								<div class="experience-time">
									<h2 style={{  textAlign: 'start', fontSize:20}}>2004 - 2008</h2>
									<h3>bachelor <span>of </span> computer science</h3>
								</div>
								<div class="timeline-horizontal-border">
									<i class="fa fa-circle" aria-hidden="true"></i>
									<span class="single-timeline-horizontal"></span>
								</div>
								<div class="timeline">
									<div class="timeline-content">
										<h4 class="title"  style={{  textAlign: 'start', fontSize:20}}>
											university of north carolina
										</h4>
										<p  style={{  textAlign: 'start', fontSize:14}}>north carolina, USA</p>
										<p class="description">
											Duis aute irure dolor in reprehenderit in vol patate velit esse cillum dolore eu fugiat nulla pari. Excepteur sint occana inna tecat cupidatat non proident. 
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="single-horizontal-timeline">
								<div class="experience-time">
									<h2 style={{  textAlign: 'start', fontSize:18}}>2004 - 2008</h2>
									<h3>bachelor <span>of </span> creative design</h3>
								</div>
								<div class="timeline-horizontal-border">
									<i class="fa fa-circle" aria-hidden="true"></i>
									<span class="single-timeline-horizontal spacial-horizontal-line
									"></span>
								</div>
								<div class="timeline">
									<div class="timeline-content">
										<h4 class="title"  style={{  textAlign: 'start', fontSize:20}}>
											university of bolton
										</h4>
										<p  style={{  textAlign: 'start', fontSize:14}}>bolton, united kingdome</p>
										<p class="description">
											Duis aute irure dolor in reprehenderit in vol patate velit esse cillum dolore eu fugiat nulla pari. Excepteur sint occana inna tecat cupidatat non proident. 
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>
    </div>
  )
}
