import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Navbars() {
  return (
    <div className='container-fluid p-0 m-0'>
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top " style={{ boxShadow: 'inset 0 1px 0 rgb(255 255 255 / 10%)', borderTop: '1px solid transparent'}}>
        <div className='container'>
            <NavLink to="/"
                style={({ isActive }) => ({
                    color: isActive ? '#d92cf9' : '#d92cf9',
                    fontSize: 24, textDecoration: 'none', fontWeight: 'bold'
                })}>
                BROWNY</NavLink>

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end " id="navbarNav" >
                <ul className="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li className=" menu active m-4"></li>
                    <li className=" menu m-4">
                        <NavLink to="/education"
                            style={({ isActive }) => ({
                                color: isActive ? '#d92cf9' : '#6f6f6f',
                                fontSize: 16, textDecoration: 'none'
                            })}>
                            EDUCATION</NavLink>
                    </li>

                    <li className="menu m-4">
                        <NavLink to="/skills"
                            style={({ isActive }) => ({
                                color: isActive ? '#d92cf9' : '#6f6f6f',
                                fontSize: 16, textDecoration: 'none'
                            })}>COMPETENCES</NavLink>
                    </li>

                    <li className="menu m-4">
                        <NavLink to="/experience"
                            style={({ isActive }) => ({
                                color: isActive ? '#d92cf9' : '#6f6f6f',
                                fontSize: 16, textDecoration: 'none'
                            })}>EXPERIENCE</NavLink></li>

                    <li className="menu m-4">
                        <NavLink to="/portfolio"
                            style={({ isActive }) => ({
                                color: isActive ? '#d92cf9' : '#6f6f6f',
                                fontSize: 16, textDecoration: 'none'
                            })}> PORTFOLIO</NavLink>
                    </li>
                    <li className="menu m-4">
                        <NavLink to="/contact"
                            style={({ isActive }) => ({
                                color: isActive ? '#d92cf9' : '#6f6f6f',
                                fontSize: 16, textDecoration: 'none'
                            })}>CONTACT</NavLink></li>
                </ul>
            </div>
        </div>
    </nav>
  

</div>
  )
}
