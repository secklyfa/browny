import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import '../style.css'
import About from './About'
import Contact from './Contact'
import Education from './Education'
import Experience from './Experience'
import Portfolio from './Portfolio'
import Skills from './Skills'



export default function Accueil() {
		return (
			<div className='container-fluid p-0 m-0 mt-5'>
   <section className='en-tete mt-5'>
					<div  class="container mt-5">
						<div  class="row">
							<div class="col-md-12 text-center " style={{zIndex:2, marginBottom:10}}>
								<div class="header-text">
									<h2 className='title ' >
									HI<span> , </span> I AM <br/> BROWNY <br/> STAR <span>.</span>
										 </h2>
										 <p class="design" style={{fontSize:20, textTransform:'uppercase', color:'white'}} >ui/ux designer and web developer</p>
							<a href="assets/download/browney.txt" download="" class="animated fadeInDown" 
							style={{height:100, width:120, color:'white', background:'#d92cf9', textDecoration:'none',
							 fontSize:18, fontWeight:'bold', paddingLeft:10,paddingRight:10,paddingTop:10, paddingBottom:10,borderRadius:5}}>download resume</a>
								</div>
							</div>
						</div>

					</div>
				</section>
				<br/>
				<br/>
				<About/>

			<br/>
				<Education/>
				<br/>
				<Skills/>
				<br/>
				<Portfolio/>
				<br/>
				<Experience/>
				<Contact/>

			</div>


			


		)
	}


