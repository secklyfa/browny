
import './App.css';
import Accueil from './Components/Accueil';
import { Routes, Route } from 'react-router-dom';
import Education from './Components/Education';
import Skills from './Components/Skills';
import Portfolio from './Components/Portfolio';
import Contact from './Components/Contact';
import Experience from './Components/Experience';
import Navbars from './Components/Navbars';
import About from './Components/About';

function App() {
  return (
    <div className="App" style={{overflow:'hidden'}}>
   <Navbars/>
      <Routes> 
        <Route path='/' element={<Accueil/>}/>
        <Route path='/about' element={<About/>}/>
        <Route path='/education' element={<Education/>}/>
        <Route path='/skills' element={<Skills/>}/>
        <Route path='/portfolio' element={<Portfolio/>}/>
        <Route path='/contact' element={<Contact/>}/>
        <Route path='/experience' element={<Experience/>}/>
      </Routes>
    
    </div>
  );
}

export default App;
